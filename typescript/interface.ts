(() => {
    
    interface Xmen {
        nombre: string;
        edad: number;
        poder?: string;
    }

    const enviarMision = ( xmen: Xmen) => {
        console.log(`Enviando a ${xmen.nombre} a la misión`);
    }
    const regresarCuartel = ( xmen: Xmen) => {
        console.log(`${xmen.nombre} vuelve de la misión`);
    }

    const wolverin: Xmen = {
        nombre: 'Logan',
        edad: 40
    }

    enviarMision(wolverin);
    regresarCuartel(wolverin);

})();



