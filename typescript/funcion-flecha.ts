(function () {

    //Funcion asignada a variable
    let miFuncion = function(a:string){
        return a;
    }

    const miFuncionF = (a:string) => a;

    const sumaN = function(a:number, b:number){
        return a+b;
    };

    const sumaF = (a:number, b:number) => a+b;

    const hulk = {
        nombre: 'Hulk',
        smash(){
            
            setTimeout( () => {
                console.log(`${this.nombre} smash!!!`);
            },1000);
        }
    }
    
    hulk.smash();
    

    /*Funcion tradicional
    function funcion2(a:string){
        return a;
    }*/

}());



